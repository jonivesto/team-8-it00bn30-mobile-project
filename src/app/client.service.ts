import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

/*
 * This service handles all GET requests
 *
 * @author: Joni-Pekka Vesto <n7vejo00@students.oamk.fi>
 * @since 22-3-1019
 */

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http:HttpClient) { }

  /*
   * Make GET request
   * @param url
   * @param sub directory (can be null)
   * @param params (can be null)
   * @return parsed JSON response object
   */
  get(url, sub, params): Observable<any> {
    // Bind subdirectory to url
    if(sub) url += sub + '/';
    // Bind params to the url
    if(params) url += this.buildParams(params);
    // Send
    return this.http.get(url);
  }

  /*
   * Attach parameters to the url to be used in GET request
   * @param parameters as object
   * @return string of parameters
   */
  buildParams(params): string {
    // Read params object
    var keys = Object.keys( params );
    var values = Object.values( params );
    // Quit if there is no keys
    if(keys.length == 0) return '';
    // Build in this
    var stringParams = '?';
    // Build
    for(let i = 0; i < keys.length; i++){
      stringParams += keys[i] + '=' + values[i];
      // Add '&' between
      if(i != keys.length-1) stringParams += '&';
    }
    // Debug
    //console.log('URL params: '+stringParams);
    return stringParams;
  }
}
