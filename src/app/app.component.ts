import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Environment } from '@ionic-native/google-maps';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Settings',
      url: '/settings',
      icon: 'settings'
    },
    {
      title: 'API playground',
      url: '/debug-tools',
      icon: 'bug'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {

      Environment.setEnv({
        // api key for server
        'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyCrYtiIipK-zHeGQIDHq5jBdJFQasF24Gg',
        // api key for local development
        'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyAbikXBJtN_XfWxOKvFWKvj5U6dhzoKdlY'
      });

      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
