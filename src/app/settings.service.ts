import { Storage } from '@ionic/storage';
import { defaultSettings, themes } from './../environments/environment';

import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { DomController } from '@ionic/angular';

interface Theme { styles: ThemeStyle[]; }
interface ThemeStyle { themeVariable: string; value: string; }

/*
 * @author: Joni-Pekka Vesto <n7vejo00@students.oamk.fi>
 * @since 26-3-1019
 */

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  // Settings (runtime only)
  temp = defaultSettings;

  // Themes
  lightTheme: Theme = themes.light;
  darkTheme: Theme = themes.dark;

  // These are for passing data between tabs in realtime
  currentCity = '';
  currentAqiLevel: any = {};
  locationData: any;
  airQualityData: any;

  lastSearch:string;

  constructor(
    private storage: Storage,
    private domCtrl: DomController,
    @Inject(DOCUMENT) private document) {
      // Load settings
      this.load();
    }

  /*
   * Get settings from the storage
   * Use defaults if there are no saved settings
   */
  load(): void {
    // Load settings from Storage
    this.storage.get('settings').then((values) => {
      // Apply loaded settings from storage
      if(values) this.temp = JSON.parse(values);
      // If there are no settings in storage
      else console.log('Using default settings.');
      // Debug
      console.log(this.temp);
    });
  }

  /*
   * Save settings to storage
   */
  save(): void {
    // Convert JSON to string
    let save = JSON.stringify(this.temp);
    // Save settings as string
    this.storage.set('settings', save);
    // Debug
    console.log('Save');
  }

  /*
   * Set app theme
   * @param useDarkTheme
   */
  setTheme(useDarkTheme: boolean): void {
    // Get theme to assign
    let theme = (useDarkTheme) ? this.darkTheme : this.lightTheme;
    // Assign theme
    this.domCtrl.write(() => {
      // Assign each property
      theme.styles.forEach(style => {
        document.documentElement.style.setProperty(style.themeVariable, style.value);
      });

    });

  }
}
