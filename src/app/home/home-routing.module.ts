import { MapPage } from '../map/map.page';
import { OverviewPage } from '../overview/overview.page';
import { StatsPage } from '../stats/stats.page';
import { NgModule } from '@angular/core';
import { HomePage } from '../home/home.page';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    component: HomePage,
    children: [
      {
        path: 'overview',
        children: [
          {
            path: '',
            loadChildren: '../overview/overview.module#OverviewPageModule'
          }
        ]
      },
      {
        path: 'stats',
        children: [
          {
            path: '',
            loadChildren: '../stats/stats.module#StatsPageModule'
          }
        ]
      },
      {
        path: 'map',
        children: [
          {
            path: '',
            loadChildren: '../map/map.module#MapPageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/home/overview',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/home/overview',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class HomePageRoutingModule {}
