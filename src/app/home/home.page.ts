import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service';
import { environment, aqiLevels } from '../../environments/environment';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { SettingsService } from '../settings.service';
import { ModalController } from '@ionic/angular';
import { CitySearchPage } from '../city-search/city-search.page';
import { ErrorMessagePage } from '../error-message/error-message.page';
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

/*
 * @author: Joni-Pekka Vesto <n7vejo00@students.oamk.fi>
 * @author: Li Lan <c7lali00@students.oamk.fi>
 * @since 22-3-1019
 * @updated 26-3-1019
 */

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  // Current data to be displayed
  currentCity = '';
  currentAqiLevel: any = {};

  // Temp. Async data is saved in these variables
  locationData: any;
  airQualityData: any;

  // Loading screen object
  loading: any;

  constructor(
    private storage: Storage,
    private client: ClientService,
    private geolocation: Geolocation,
    private settings: SettingsService,
    public modalController: ModalController,
    public loadingController: LoadingController,
    private router: Router ){
  }

  /*
   * Run on page load
   */
  ngOnInit(){
    // Get aqi
    if(this.settings.lastSearch) this.qualityByCity(this.settings.lastSearch);
    else this.qualityByCity(this.settings.temp.homeCity);

    // Show loading screen
    //this.presentLoading();

    // Hide loading screen
    //if(this.loading) this.loading.dismiss();

    // Autocomplete (debug)
    //this.autoComplete("hels");

    // Show error message
    //this.errorMessage("This is test error message");

    // Set theme
    setTimeout(() => {
     this.settings.setTheme(this.settings.temp.useDarkTheme);
    }, 350);
  }

  /*
   * Locate the device using Geolocation
   * The response data is saved in 'locationData'
   */
  getCurrentLocation(): void {
    this.geolocation.getCurrentPosition().then((response) => {
      // Handle response
      this.locationData = response;
      this.settings.locationData = response;
      // Get aqi for the response location
      this.qualityByCoords();
      // Debug
      console.log(response);
    // Handle errors
    }).catch((error) => {
      console.error(error);
    });
  }

  /*
   * Get air quality data by lat/lon value
   * The response data is saved in 'airQualityData'
   */
  qualityByCoords(): void {
    // Show loading
    this.presentLoading();

    // Params for the GET request
    var params = {
      token: environment.airQualityApiToken
    };

    // Subdirectory for the GET request
    let lat = this.locationData.coords.latitude;
    let lon = this.locationData.coords.longitude;
    var sub = 'geo:' + lat + ';' + lon;

    // Make GET request
    this.client.get(environment.airQualityApiUrl, sub, params)
    .subscribe(data => {
      // Set timestamp
      data.timestamp = new Date().toLocaleString();
      // Get response
      this.airQualityData = data;
      this.settings.airQualityData = data;
      // Debug
      console.log(data);
      // Get aqiLevels
      this.getCurrentAqiLevel();
      // Hide loading screen
      setTimeout(() => {
       if(this.loading) this.loading.dismiss();
     }, 350);
    });
  }

  /*
   * Get air quality data by city name
   *
   * IMPORTANT: If possible, use the lat/lon based search because it always
   * finds results. This can return error: "unknown city name"
   *
   * @param city name to search aqi
   * The response data is saved in 'airQualityData'
   */
  qualityByCity(city: string): void {
    // save last search
    this.settings.lastSearch = city;
    // Show loading screen
    this.presentLoading();
    // Params for the GET request
    var params = {
      token: environment.airQualityApiToken
    };
    // Make GET request
    this.client.get(environment.airQualityApiUrl, city, params)
    .subscribe(data => {
      // Hande errors
      if(data.status == 'error') {
        // Show error message
        this.errorMessage('We do not know the air quality of this city.');
        // Get aqi with coords
        this.qualityByCoords();
      // Else if data was successfully received
      } else {
        // Set timestamp
        data.timestamp = new Date().toLocaleString();
        // Get response
        this.airQualityData = data;
        this.settings.airQualityData = data;
        // Debug
        console.log(data);
        // Get aqiLevels
        this.getCurrentAqiLevel();
      }
      // Hide loading screen
      setTimeout(() => {
       if(this.loading) this.loading.dismiss();
     }, 350);
    });
  }

  /*
   * Display city search as modal
   */
  async searchModal() {
    // Value of 'sort' is passed to modal using the navParams
    const modal = await this.modalController.create({
      component: CitySearchPage,
      componentProps: { sort: this.settings.temp.sortLargeCitiesFirst }
    });
    // Getting data from the modal:
    modal.onDidDismiss().then((response) => {
      // City name
      try{
        // Get city name
        let city = response.data.result.split(', ')[0];
        // Set current city
        //this.currentCity = city;
        //this.settings.currentCity = city;
        // Get aqi in that city
        this.qualityByCity(city);
      }
      catch{
        // Debug
        console.log('searchModal did not return anything');
      }
    });
    // Display modal
    return await modal.present();
  }

  /*
   * Loading screen
   */
  async presentLoading() {
    // Create loading screen
    this.loading = await this.loadingController.create({
      message: 'Please wait',
      duration: 2000
    });
    // Display it
    await this.loading.present();
    // Return data
    const { role, data } = await this.loading.onDidDismiss();
    // Debug
    console.log('Loading finished!');
  }

  /*
   * Get current matching aqi info from environment.ts
   */
  getCurrentAqiLevel(): void {
    // Go to home page
    this.router.navigate([''])
    // Get aqi difference
    this.getAqiDifference();
    // Set City
    if (this.airQualityData.data.city.name.indexOf(',') > -1) this.currentCity = this.airQualityData.data.city.name.split(', ')[1];
    else this.currentCity = this.airQualityData.data.city.name;
    this.settings.currentCity = this.currentCity;

    try{
      // Get aqi
      var aqi = this.airQualityData.data.aqi;
      var lvl = {};
      // Get level
      aqiLevels.forEach( function(level) {
        // Check if aqi is in range
        if(aqi >= level.min && aqi <= level.max){
          // Debug
          console.log(level);
          // Save level
          lvl = level;
        }
      });
      // Set level
      this.currentAqiLevel = lvl;
      this.settings.currentAqiLevel = lvl;
    }
    catch (error){
      // Quit
      return;
    }
  }

  /*
   * Display error message
   */
  async errorMessage(message: string) {
    // Value of 'sort' is passed to modal using the navParams
    const modal = await this.modalController.create({
      component: ErrorMessagePage,
      componentProps: { message: message }
    });
    // Display modal
    return await modal.present();
    // Hide loading
    setTimeout(() => {
     if(this.loading) this.loading.dismiss();
   }, 459);

  }

  /*
   * Get old datas from storage and compare them to current aqi
   */
  getAqiDifference(): void{
    var difference = 0;
    var increased: boolean;
    var datas = [];
    var aqi = this.airQualityData.data.aqi;
    // Get older aqi's
    this.storage.get('aqi_archive').then((values) => {
      // if there is data in storage
      if(values){
        // Parse json
        datas = JSON.parse(values);
        // Find older datas from same station
        datas.forEach(function(data){
          if(this.airQualityData.data.city.name == data.data.city.name){
            difference = (aqi > data.data.aqi)? aqi-data.data.aqi : data.data.aqi-aqi;
            increased = aqi > data.data.aqi;
            // Save in temp data
            this.settings.airQualityData.diff = difference;
            this.settings.airQualityData.increased = increased;
          }
        }, this);
      } else console.log('No data to compare');

      // Trim datas length before save
      while(datas.length > 50){
        datas.splice(-1,1);
      }
      // Debug
      console.log('aqi_archive:', datas);
      console.log('aqi_difference', difference)
      // Save current datas
      datas.unshift(this.airQualityData);
      this.storage.set('aqi_archive', JSON.stringify(datas));
    });

  }

}
