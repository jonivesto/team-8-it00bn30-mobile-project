import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitySearchPage } from './city-search.page';

describe('CitySearchPage', () => {
  let component: CitySearchPage;
  let fixture: ComponentFixture<CitySearchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitySearchPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitySearchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
