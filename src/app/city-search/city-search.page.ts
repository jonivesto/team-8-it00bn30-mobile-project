import { Component, ViewChild } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { environment } from '../../environments/environment';
import { ClientService } from '../client.service';
import { ModalController } from '@ionic/angular';

/*
 * @author: Joni-Pekka Vesto <n7vejo00@students.oamk.fi>
 * @since 27-3-1019
 */

@Component({
  selector: 'app-city-search',
  templateUrl: './city-search.page.html',
  styleUrls: ['./city-search.page.scss'],
})
export class CitySearchPage {

  @ViewChild('search') search;

  autocompleteData = [];
  sortLargeCitiesFirst: boolean;

  constructor(navParams: NavParams, private client: ClientService, private modalController: ModalController) {
    // componentProps can be accessed at construction time using NavParams
    this.sortLargeCitiesFirst = navParams.get('sort');
    // Focus to text input
    setTimeout(() => {
      this.search.setFocus();
    },150);
  }

  /*
   * Autocomplete city names
   * @param search term
   * The response data is saved in 'autoCompleteData'
   */
  autoComplete(term: string): void {
    // Do not search with too short term
    if(term.length < 3){
      // Clear results
      this.autocompleteData = [];
      // Quit
      return;
    }
    // Params for the GET request
    var params: any = {
      q: term
    };
    // City sort method
    if(this.sortLargeCitiesFirst) params.sort = 'size';
    // Make GET request
    this.client.get(environment.autoCompleteApiUrl, null, params)
    .subscribe(data => {
      // Get response
      if(data[0]=="")data = [];
      this.autocompleteData = data;
      // Debug
      console.log(data);
    });
  }

  /*
   * Click handler for search results
   */
  selectResult(index){
    // Quit with selected result
    this.modalController.dismiss({
      'result': this.autocompleteData[index]
    });
  }

  /*
   * Close modal page
   */
  cancelSearch(){
    // Quit
    this.modalController.dismiss({});
  }
}
