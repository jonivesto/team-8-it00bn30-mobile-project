import { Component } from '@angular/core';
import { SettingsService } from '../settings.service';

/*
 * @author: Joni-Pekka Vesto <n7vejo00@students.oamk.fi>
 */

@Component({
  selector: 'app-overview',
  templateUrl: './overview.page.html',
  styleUrls: ['./overview.page.scss'],
})
export class OverviewPage {

  constructor( private settings: SettingsService ) { }

}
