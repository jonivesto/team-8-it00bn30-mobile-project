import { Component } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { ModalController } from '@ionic/angular';

/*
 * @author: Joni-Pekka Vesto <n7vejo00@students.oamk.fi>
 * @since 30-3-1019
 */

@Component({
  selector: 'app-error-message',
  templateUrl: './error-message.page.html',
  styleUrls: ['./error-message.page.scss'],
})
export class ErrorMessagePage {

  message = 'Something went wrong';

  constructor(navParams: NavParams, private modalController: ModalController) {
    // componentProps can be accessed at construction time using NavParams
    this.message = navParams.get('message');
  }

  accept(){
    this.modalController.dismiss();
  }
}
