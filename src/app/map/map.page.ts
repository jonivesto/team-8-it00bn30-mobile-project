import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../settings.service';
import { Platform } from '@ionic/angular';
import { GoogleMaps, GoogleMap, GoogleMapsEvent, Marker, GoogleMapsAnimation, MyLocation } from '@ionic-native/google-maps';

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {

  map: GoogleMap;
  address:string;
  loadError = false;
  firstLoad = true;
  markers = [];

  constructor(
    private platform: Platform,
    private settings: SettingsService ) {
  }

  ngOnInit(){
    // Wait
    this.platform.ready();
    // Check that data is received
    if(this.settings.airQualityData) this.loadMap();
    // If not, show error
    else this.loadError = true;
    // Set first load
    this.firstLoad = false;
  }

  ionViewDidEnter() {
    // Quit on first load
    if(this.firstLoad) return;
    // Remove markers
    this.markers.forEach( function(marker) {
      marker.setVisible(false);
    });

    // Move the map camera to the location with animation
    this.map.animateCamera({
      target: {
        lat: this.settings.airQualityData.data.city.geo[0],
        lng: this.settings.airQualityData.data.city.geo[1]
      },
      zoom: 13,
      duration: 5000
    });

    this.myLocation();
  }

  loadMap() {

    // Create map
    this.map = GoogleMaps.create('map_canvas', {
      camera: {
         target: {
           lat: this.settings.airQualityData.data.city.geo[0],
           lng: this.settings.airQualityData.data.city.geo[1]
         },
         zoom: 13,
         tilt: 30
       }
    });

    this.myLocation();

  }

  myLocation(){
    // Add a marker
    let marker: Marker = this.map.addMarkerSync({
      title: 'Air quality index: ' + this.settings.airQualityData.data.aqi,
      snippet: 'Station: ' + this.settings.airQualityData.data.city.name,
      position: {
        lat: this.settings.airQualityData.data.city.geo[0],
        lng: this.settings.airQualityData.data.city.geo[1]
      }
    });
    this.markers.push(marker);
    // Show marker info
    marker.showInfoWindow();
  }

  /*
  goToMyLocation(){
    this.map.clear();

    // Get the location of you
    this.map.getMyLocation().then((location: MyLocation) => {
      console.log(JSON.stringify(location, null ,2));

      // Move the map camera to the location with animation
      this.map.animateCamera({
        target: location.latLng,
        zoom: 17,
        duration: 5000
      });

      //add a marker
      let marker: Marker = this.map.addMarkerSync({
        title: 'Your location',
        snippet: 'hello there',
        position: location.latLng,
        animation: GoogleMapsAnimation.BOUNCE
      });

      //show the infoWindow
      marker.showInfoWindow();

      //If clicked it, display the alert
      marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
        this.showToast('clicked!');
      });

      this.map.on(GoogleMapsEvent.MAP_READY).subscribe(
        (data) => {
            console.log("Click MAP",data);
        }
      );
    })
    .catch(err => {
      //this.loading.dismiss();
      this.showToast(err.error_message);
    });
  }

  async showToast(message: string) {
    let toast = await this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'middle'
    });
    toast.present();
  }*/
}
