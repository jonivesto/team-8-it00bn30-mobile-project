import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DebugToolsPage } from './debug-tools.page';

describe('DebugToolsPage', () => {
  let component: DebugToolsPage;
  let fixture: ComponentFixture<DebugToolsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DebugToolsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DebugToolsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
