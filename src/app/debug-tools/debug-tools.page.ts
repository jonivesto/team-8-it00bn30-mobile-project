import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../settings.service';
import { environment } from '../../environments/environment';
import { ClientService } from '../client.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';


/*
 * @author: Joni-Pekka Vesto <n7vejo00@students.oamk.fi>
 * @author: Li Lan <c7lali00@students.oamk.fi>
 * @since 28-3-2019
 * @updated 30-3-2019
 */

@Component({
  selector: 'app-debug-tools',
  templateUrl: './debug-tools.page.html',
  styleUrls: ['./debug-tools.page.scss'],
})
export class DebugToolsPage implements OnInit {
  cityName:string;
  latitude:string;
  longitude:string;
  geolocationData:any;
  geolocationDataStr:string;
  airQualityData: any;
  airQualityDataStr:string;
  autoCompleteTerm:string;
  autoCompleteData :any;
  autoCompleteDataStr:string;


  constructor(
    private client: ClientService,
    private settings: SettingsService,
    private geolocation: Geolocation,
   ) { }

  ngOnInit() {

  }

  doAutoComplete(){
     // Params for the GET request
     var params: any = {
      q: this.autoCompleteTerm
    };
    // City sort method
    params.sort = 'size';
    // Make GET request
    this.client.get(environment.autoCompleteApiUrl, null, params)
    .subscribe(data => {
      // Get response
      this.autoCompleteData = data;
      this.autoCompleteDataStr = JSON.stringify(data,null,'\t'); 
      // Debug
      console.log(data);
    });
  }

  onAutoCompleteChange(){
    if (this.autoCompleteTerm == '')
    this.autoCompleteDataStr = '';
  }

  getAirQualityByCity(): void{
     // Params for the GET request
     var params = {
       token: environment.airQualityApiToken
     };
     // Make GET request
     this.client.get(environment.airQualityApiUrl, this.cityName, params)
     .subscribe(data => {
         // Get response
         this.airQualityData = data;
         this.airQualityDataStr = JSON.stringify(data,null,'\t');
         // Debug
         console.log(data);
     });
  }

  onCityNameChange(){
    if (this.cityName == '')
    this.airQualityDataStr = '';
  }

  getAirQualityByCoords(){
     // Params for the GET request
     var params = {
      token: environment.airQualityApiToken
    };
     var sub = 'geo:' + this.latitude + ';' + this.longitude;
    // Make GET request
    this.client.get(environment.airQualityApiUrl, sub, params)
    .subscribe(data => {
        // Get response
        this.airQualityData = data;
        this.airQualityDataStr = JSON.stringify(data, null, '\t');
        // Debug
        console.log(data);
    });
  }

  onLatitudeChange(){
    if (this.latitude == '')
    this.airQualityDataStr = '';
  }

  onLongitudeChange(){
    if (this.longitude == '')
    this.airQualityDataStr = '';
  }



  getGeolocation(){
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log(resp);
      this.geolocationData = resp;
      var obj = this.geoPositionToObject(resp);
      this.geolocationDataStr = JSON.stringify(obj,null,'\t');
     }).catch((error) => {
       console.log('Error getting location'+ JSON.stringify(error));
     });

  }

  geoPositionToObject(geoposition) {
    return {
      timestamp: geoposition.timestamp,
      coords: {
        accuracy: geoposition.coords.accuracy,
        altitude: geoposition.coords.altitude,
        altitudeAccuracy: geoposition.coords.altitudeAccuracy,
        heading: geoposition.coords.heading,
        latitude: geoposition.coords.latitude,
        longitude: geoposition.coords.longitude,
        speed: geoposition.coords.speed
      }
    }
  }

}
