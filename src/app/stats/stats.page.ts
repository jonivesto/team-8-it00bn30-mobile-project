import { Component, ViewChild } from '@angular/core';
import { SettingsService } from '../settings.service';
import { Chart } from 'chart.js';

/*
 * @author: Joni-Pekka Vesto <n7vejo00@students.oamk.fi>
 */

@Component({
  selector: 'app-stats',
  templateUrl: './stats.page.html',
  styleUrls: ['./stats.page.scss'],
})
export class StatsPage {

  @ViewChild('barCanvas') barCanvas;

  barChart: any;
  loadError = false;

  constructor( private settings: SettingsService ) { }

  /*
   * Run on page load
   */
  ionViewDidEnter() {
      // Check if there is data to be used in chart
      if(!this.settings.airQualityData){
        this.loadError = true;
        return;
      }
      this.barChart = undefined;
      // Draw bar chart using Chart.js
      // See documentation here: https://www.chartjs.org/docs/latest/
      this.barChart = new Chart(this.barCanvas.nativeElement, {
          type: 'bar',
          data: {
            labels: ["PM 2.5", "PM 10", "NO2"],
            datasets: [{
                label: 'Indivudal AQI',
                data: [
                  this.settings.airQualityData.data.iaqi.pm25.v,
                  this.settings.airQualityData.data.iaqi.pm10.v,
                  this.settings.airQualityData.data.iaqi.no2.v
                ],
                backgroundColor: 'rgba(130, 200, 255, 0.4)',
                borderColor: 'rgba(130, 200, 255, 0.8)',
                borderWidth: 1
            }]
          },
          options: {
            legend: {
                labels: {
                    // This more specific font property overrides the global property
                    fontColor: (this.settings.temp.useDarkTheme)? 'rgba(207, 205, 205, 0.986)':'black'
                }
            },
            scales: {

                xAxes:[{
                  gridLines:{
                     color:(this.settings.temp.useDarkTheme)? 'rgba(124, 122, 122, 0.986)':'rgba(1,1,1,0)'
                   },
                   ticks: {
                    fontColor:(this.settings.temp.useDarkTheme)? 'rgba(200, 200, 200, 1)':'rgba(0,0,0,1)'
                  }
                 }],

                yAxes: [{
                 gridLines:{
                  color:(this.settings.temp.useDarkTheme)? 'rgba(124, 122, 122, 0.986)':'rgba(0,0,0,0.1)'
                  },
                    ticks: {
                     fontColor:(this.settings.temp.useDarkTheme)? 'rgba(200, 200, 200, 1)':'rgba(0,0,0,1)',
                     beginAtZero:true
                    }
                }]
            }
          }
      });
    }

}
