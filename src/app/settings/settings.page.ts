import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../settings.service';
import { Storage } from '@ionic/storage';
import { ToastController } from '@ionic/angular';

/*
 * @author: Joni-Pekka Vesto <n7vejo00@students.oamk.fi>
 * @since 26-3-1019
 */

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  // Ng references
  useDarkTheme:boolean;
  sortLargeCitiesFirst:boolean;
  homeCity:string;

  constructor(
    private settings: SettingsService,
    private storage: Storage,
    public toastController: ToastController ){
  }

  ngOnInit(){
    // Set theme based on settings
    this.useDarkTheme = this.settings.temp.useDarkTheme;
    this.sortLargeCitiesFirst = this.settings.temp.sortLargeCitiesFirst;
    this.homeCity = this.settings.temp.homeCity;
  }

  /*
   * Change theme
   * This is called when the toggle input value changes
   */
  changeTheme(){
    // Set theme
    this.settings.setTheme(this.useDarkTheme);
    // Make changes to the runtime settings
    this.settings.temp.useDarkTheme = this.useDarkTheme;
    // Save changes
    this.settings.save();
  }

  /*
   * Change sort method
   * This is called when the select input value changes
   */
  changeCitySort(){
    // Make changes to the runtime settings
    this.settings.temp.sortLargeCitiesFirst = this.sortLargeCitiesFirst;
    // Save changes
    this.settings.save();
  }

  changeHomeCity(){
    // Make changes to the runtime settings
    this.settings.temp.homeCity = this.homeCity;
    // Save changes
    this.settings.save();
  }

  /*
   * Clear all data in local storage
   */
  clearAllData(){
    // Clear all data
    this.storage.clear();
    this.settings.lastSearch = undefined;
    // Toast
    this.presentToast('Success! Restart the app to take full effect.');
  }

  /*
   * Show toast message
   */
  async presentToast(message: string) {
    // Prepare
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    // Show
    toast.present();
  }
}
