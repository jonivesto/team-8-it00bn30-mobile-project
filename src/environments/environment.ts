// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  airQualityApiUrl:'https://api.waqi.info/feed/',
  airQualityApiToken:'bd4040f1f84d1a2eb9f0ccaaf01a83fc0471cb5c',
  autoCompleteApiUrl: 'https://cors-anywhere.herokuapp.com/http://autocompletecity.geobytes.com/AutoCompleteCity'
};

/*
 *
 */
export const defaultSettings = {
  homeCity: 'Oulu',
  useDarkTheme: false,
  sortLargeCitiesFirst: true
};

/*
 *
 */
export const themes = {
  light: {
    styles: [
      { themeVariable: '--ion-background-color', value: ' #fff'},
      { themeVariable: '--ion-text-color', value: '#000'}
    ]
  },
  dark: {
    styles: [
      { themeVariable: '--ion-background-color', value: ' #151515'},
      { themeVariable: '--ion-text-color', value: '#fff'}
    ]
  }
};

/*
 *
 */
export const aqiLevels = [
  {
    status: 'Good',
    min: 0,
    max: 50,
    desc: 'Air quality is considered satisfactory',
    color: 'green',
    longDesc: 'Air quality is considered satisfactory, and air pollution poses little or no risk',
    caution: 'None'
  },
  {
    status: 'Moderate',
    min: 51,
    max: 100,
    desc: 'Air quality is acceptable',
    color: 'yellow',
    longDesc: 'Air quality is acceptable; however, for some pollutants there may be a moderate health concern for a very small number of people who are unusually sensitive to air pollution.',
    caution: 'Active children and adults, and people with respiratory disease, such as asthma, should limit prolonged outdoor exertion.'
  },
  {
    status: 'Unhealthy for sensitive groups',
    min: 101,
    max: 150,
    desc: 'Members of sensotive groups may experience health effects',
    color: 'orange',
    longDesc: 'Members of sensitive groups may experience health effects. The general public is not likely to be affected.',
    caution: 'Active children and adults, and people with respiratory disease, such as asthma, should limit prolonged outdoor exertion.'
  },
  {
    status: 'Unhealthy',
    min: 151,
    max: 200,
    desc: 'Everyone may begin to experience health affects',
    color: 'red',
    longDesc: 'Everyone may begin to experience health effects; members of sensitive groups may experience more serious health effects',
    caution: 'Active children and adults, and people with respiratory disease, such as asthma, should avoid prolonged outdoor exertion; everyone else, especially children, should limit prolonged outdoor exertion'
  },
  {
    status: 'Very unhealthy',
    min: 201,
    max: 300,
    desc: 'Health warnings of emergency conditions',
    color: 'violet',
    longDesc: 'Health warnings of emergency conditions. The entire population is more likely to be affected.',
    caution: 'Active children and adults, and people with respiratory disease, such as asthma, should avoid all outdoor exertion; everyone else, especially children, should limit outdoor exertion.'
  },
  {
    status: 'Hazardous',
    min: 301,
    max: 9999,
    desc: 'Everyne may experience more serious health effects',
    color: 'brown',
    longDesc: 'Health alert: everyone may experience more serious health effects',
    caution: 'Everyone should avoid all outdoor exertion'
  },
];
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
